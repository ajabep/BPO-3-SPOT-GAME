package jeu;

import org.junit.Test;

import static org.junit.Assert.*;
import static jeu.Couleur.*;

public class PieceTest {

  @Test
  public void testGetCouleur() throws Exception {
    Position[] pos = new Position[2];
    pos[0] = new Position(1, 2);
    pos[1] = new Position(2, 2);

    Couleur c = BLANC;

    Piece p = new Piece(pos, c);

    assertEquals(p.getCouleur(), BLANC);
  }

  @Test
  public void testGetPos() throws Exception {
    Position[] pos = new Position[2];
    pos[0] = new Position(1, 2);
    pos[1] = new Position(2, 2);

    Couleur c = BLANC;

    Piece p = new Piece(pos, c);

    assertArrayEquals(p.getPos(), pos);
  }

  @Test
  public void testSetPos() throws Exception {
    Position[] pos = new Position[2];
    pos[0] = new Position(1, 2);
    pos[1] = new Position(2, 2);

    Couleur c = BLANC;

    // on initialise une pi�ce blanche
    Piece p = new Piece(pos, c);

    //on test ses positions
    assertArrayEquals(p.getPos(), pos);
    assertArrayEquals(p.getPos(), pos);


    pos[0] = new Position(2687, 2458);
    pos[1] = new Position(2687, 2457);

    // on le d�place et on tests les positions
    p.setPos(pos);

    assertArrayEquals(p.getPos(), pos);
    assertArrayEquals(p.getPos(), pos);


    pos[0] = new Position(2687, 24);
    pos[1] = new Position(2687, 2457);
    try {
      p.setPos(pos);
      fail();
    }
    catch (Exception e) {}


    pos[0] = new Position(2687, 2457);
    pos[1] = new Position(2687, 2457);
    try {
      p.setPos(pos);
      fail();
    }
    catch (Exception e) {}


    pos = new Position[3];
    pos[0] = new Position(2687, 24);
    pos[1] = new Position(2687, 2457);
    pos[2] = new Position(287, 2457);
    try {
      p.setPos(pos);
      fail();
    }
    catch (Exception e) {}

  }

  @Test
  public void testGetNumberOfSpot() throws Exception {
    Plateau plateau = new Plateau();
    Piece p = plateau.currentPiece();
    Position[] pos = new Position[2];

    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      1
    );

    pos[0] = new Position(0, 1);
    pos[1] = new Position(0, 2);
    p.setPos(pos);
    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      0
    );

    pos[0] = new Position(1, 1);
    pos[1] = new Position(2, 1);
    p.setPos(pos);
    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      1
    );

    pos[0] = new Position(2, 1);
    pos[1] = new Position(2, 2);
    p.setPos(pos);
    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      2
    );

    pos[0] = new Position(2, 0);
    pos[1] = new Position(2, 1);
    p.setPos(pos);
    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      2
    );

    pos[0] = new Position(0, 1);
    pos[1] = new Position(1, 1);
    p.setPos(pos);
    assertEquals(
      p.getNumberOfSpot(plateau.getMatriceCellule()),
      0
    );
  }

  @Test
  public void testGetPoint() throws Exception {
    Position[] pos = new Position[2];
    pos[0] = new Position(1, 2);
    pos[1] = new Position(2, 2);

    Couleur c = BLANC;

    Plateau plateau = new Plateau();

    // on initialise une pi�ce blanche
    Piece p = new Piece(pos, c);


    pos[0] = new Position(2, 2);
    pos[1] = new Position(2, 1);

    // on le d�place et on tests les points gagn�s
    assertEquals(
      p.setPos(pos, plateau.getMatriceCellule()),
      2
    );

    // on test les points totaux
    assertEquals(
      p.getPoint(),
      2
    );


    pos[0] = new Position(2, 1);
    pos[1] = new Position(1, 1);

    // on le d�place et on tests les points gagn�s
    assertEquals(
      p.setPos(pos, plateau.getMatriceCellule()),
      1
    );

    // on test les points totaux
    assertEquals(
      p.getPoint(),
      3
    );


    pos[0] = new Position(0, 0);
    pos[1] = new Position(1, 0);

    // on le d�place et on tests les points gagn�s
    assertEquals(
      p.setPos(pos, plateau.getMatriceCellule()),
      0
    );

    // on test les points totaux
    assertEquals(
      p.getPoint(),
      3
    );


    pos[0] = new Position(1, 1);
    pos[1] = new Position(2, 1);

    // on le d�place et on tests les points gagn�s
    assertEquals(
      p.setPos(pos, plateau.getMatriceCellule()),
      1
    );

    // on test les points totaux
    assertEquals(
      p.getPoint(),
      4
    );
  }
}
/*
 * Created by J-B on 06/02/2015.
 */

package jeu;

public enum Cellule {
    SPOT,
    NO_SPOT;

    final static public char SPOT_CHAR = 'O';
}
package jeu;

import static org.junit.Assert.*;
import org.junit.Test;

import static jeu.Couleur.*;
import static jeu.Cellule.*;

public class PlateauTest {

  @Test
  public void testToString() {
    Plateau p = new Plateau();

    assertEquals(p.toString(), "*************\n" +
        "*   *   *   *\n" +
        "*   * R * R *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "*   * W * W *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "*   * B * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    assertEquals(p.toString(), p.toString(-1));

    int IdOfPieceToNotDisplay = 0;
    assertEquals(p.toString(IdOfPieceToNotDisplay),
      "*************\n" +
        "*   *   *   *\n" +
        "*   *   * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "*   * W * W *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "*   * B * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

  }

  @Test
  public void testGetMatriceCellule() throws Exception {
    Plateau p = new Plateau();
    Cellule[][] cellules = {
      {NO_SPOT, NO_SPOT, SPOT},
      {NO_SPOT, NO_SPOT, SPOT},
      {NO_SPOT, NO_SPOT, SPOT}
    };

    assertArrayEquals(p.getMatriceCellule(), cellules);
  }

  @Test
  // test plein d'actions
  public void testSenario() throws Exception {
    Plateau plateau = new Plateau();

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(ROUGE));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "* 3 *   * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 2 * W * W *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 1 * B * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 3);


    // on fait des positions
    try {
      plateau.makeMove(4); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(1); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());



    // on est pass� � la pi�ce blanche

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLANC));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "* 3 * 4 * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * 1 * 2 *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * B * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 4);


    // on fait des positions
    try {
      plateau.makeMove(5); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(3); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());


    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      0     |"
    );


    // on est pass� � la pi�ce bleu

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLEU));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "* W * W * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * 3 * 4 *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * 1 * 2 *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 4);


    // on fait des positions
    try {
      plateau.makeMove(5); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(2); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());



    // on est pass� � la pi�ce blanche

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLANC));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "*   * 3 * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * 2 * B *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * 1 * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 3);


    // on fait des positions
    try {
      // on fait un movement impossible
      plateau.makeMove(43574);
      fail();
    } catch (Exception e) {}

    plateau.makeMove(3); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());


    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      2     |"
    );



    // TOUR 2
    // ======




    //On en est � la pi�ce rouge maintenant

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(ROUGE));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "*   * W * W *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "*3-4*   * B *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 1 * 2 * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 4);


    // on fait des positions
    try {
      // on fait un movement impossible
      plateau.makeMove(234);
      fail();
    } catch (Exception e) {}

    plateau.makeMove(4); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());



    // on est pass� � la pi�ce blanche

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLANC));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "* 2 *   * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * R * B *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 1 *   * B *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 2);


    // on fait des positions
    try {
      plateau.makeMove(4); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(2); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());


    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      2     |"
    );



    // on est pass� � la pi�ce bleu

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLEU));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "* W * W * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * R * 3 *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 1 * 2 * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 3);


    // on fait des positions
    try {
      plateau.makeMove(4); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(3); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());



    // on est pass� � la pi�ce blanche

    // test de la pi�ce courante
    assertTrue(
      plateau.currentPiece().getCouleur().equals(BLANC));

    // on tests les positions possibles
    assertEquals(plateau.toStringWithPossiblePositions(),
      "*************\n" +
        "*   *   *   *\n" +
        "*   *   * B *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* R * R * B *\n" +
        "*   *   *   *\n" +
        "*************\n" +
        "*   *   *   *\n" +
        "* 1 * 2 * " + SPOT_CHAR + " *\n" +
        "*   *   *   *\n" +
        "*************\n"
    );

    // on test le nombre de positions possibles
    assertEquals(plateau.getNumberOfPossiblePosition(), 2);


    // on fait des positions
    try {
      plateau.makeMove(35); // on fait un movement impossible
      fail();
    } catch (Exception e) {}

    plateau.makeMove(1); // puis un possible

    // on test la fin de la partie
    assertFalse(plateau.partieEstFinie());


    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      4     |"
    );



    // on se d�p�che de finir la parite maintenant pour
    // pouvoir tester `plateau.partieEstFinie()` qd c'est
    // le cas
    plateau.makeMove(3);
    plateau.makeMove(4);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      4     |"
    );


    plateau.makeMove(3);
    plateau.makeMove(1);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      0     |\n" +
        "|     2     |      6     |"
    );


    plateau.makeMove(3);
    plateau.makeMove(1);
    plateau.makeMove(3);
    plateau.makeMove(3);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      1     |\n" +
        "|     2     |      7     |"
    );


    plateau.makeMove(1);
    plateau.makeMove(1);
    plateau.makeMove(3);
    plateau.makeMove(2);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      1     |\n" +
        "|     2     |      9     |"
    );


    plateau.makeMove(2);
    plateau.makeMove(2);
    plateau.makeMove(1);
    plateau.makeMove(3);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      1     |\n" +
        "|     2     |     11     |"
    );


    plateau.makeMove(1);
    plateau.makeMove(3);
    plateau.makeMove(3);

    // on test le r�capitulatif des points
    assertEquals(plateau.getPointsRecap(),
      "| Joueur n� |   Points   |\n" +
        "|-----------|------------|\n" +
        "|     1     |      1     |\n" +
        "|     2     |     13     |"
    );




    // la partie doit �tre finie a pr�sent
    assertTrue(plateau.partieEstFinie());

    // on test le gagnant
    assertTrue(
      plateau.getWinner().getCouleur().equals(BLEU));

  }
}
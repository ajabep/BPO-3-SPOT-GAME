/*
 * Created by J-B on 06/02/2015.
 */
package jeu;


public class Position {

  private int x;
  private int y;

  public Position(int x, int y) throws Exception {
    this.setX(x);
    this.setY(y);
  }

  public int hashCode() { // si on multiplie la mesure
    // horizontale par la limite verticale,
    // et que l'on ajoute la verticale on ne peut pas
    // obtenir de collisions
    return Plateau.LENGTH_HEIGHT * x + y;
  }

  public boolean equals(Position position) {
    return this == position
      || !(position == null
      || this.getClass() != position .getClass())
      && this.x == position.x && this.y == position.y;
  }

  public void setX(int x) throws Exception {
    if( x < 0 )
      throw new Exception("x must to be greater or equal" +
        " than 0");
    this.x = x;
  }

  public void setY(int y) throws Exception{
    if( y < 0 )
      throw new Exception("y must to be greater or equal" +
        " than 0");
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  /**
   * determine si la position courante est contigue � la
   * position p2
   * @param p2  position a tester
   * @return true si les points sont contigue, false sinon
   */
  public boolean isContiguousTo( Position p2 ) {
    return (this.x - 1 == p2.x  &&  this.y == p2.y )
      || (this.x + 1 == p2.x  &&  this.y == p2.y )
      || (this.y - 1 == p2.y  &&  this.x == p2.x )
      || (this.y + 1 == p2.y  &&  this.x == p2.x );
  }
}

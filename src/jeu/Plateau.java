/*
 * Created by J-B on 05/02/2015.
 */
package jeu;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import static jeu.Couleur.*;
import static jeu.Cellule.*;

/** G�re le plateau de jeu */
public class Plateau {

  /** patron pour l'affichage */
  private static final String patron = "*************\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*************\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*************\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*   *   *   *\n" +
    "*************\n";

  public final static int LENGTH_HEIGHT = 3;
  public final static int LENGTH_WIDTH = 3;

  private long tour;
  private ArrayList<Position[]> possiblePositions;
  private int currentPiece;

  private ArrayList<Piece> pieceListe;

  private Cellule[][] matriceCellule;

  public Plateau() {

    this.matriceCellule = new
      Cellule[LENGTH_HEIGHT][LENGTH_WIDTH];

    // g�n�re la matrice de Cellules
    for( Cellule[] ligneCellule : this.matriceCellule )
      for ( int i = 0; i < LENGTH_WIDTH ; ++i)
        if( i == LENGTH_WIDTH - 1 )
          ligneCellule[i] = SPOT;
        else
          ligneCellule[i] = NO_SPOT;

    this.pieceListe = new ArrayList<Piece>();
    // g�n�re les pi�ces
    Position[] pos = new Position[2];
    try {
      // juste pour pouvoir compiler, mais c'est
      // imposible que �a l�ve une erreur : les valeurs
      // int�gr�s sont >0
      pos[0] = new Position(1, 0);
      pos[1] = new Position(2, 0);
      this.pieceListe.add(new Piece(pos, ROUGE));

      pos[0] = new Position(1, 1);
      pos[1] = new Position(2, 1);
      this.pieceListe.add(new Piece(pos, BLANC));

      pos[0] = new Position(1, 2);
      pos[1] = new Position(2, 2);
      this.pieceListe.add(new Piece(pos, BLEU));
    } catch (Exception e){} // pour pouvoir compiler

    this.tour = -1;
    currentPiece = 1;
    this.possiblePositions = new ArrayList<Position[]>();
    this.nextTour();
  }

  public Cellule[][] getMatriceCellule() {
    return matriceCellule;
  }

  /**
   * va afficher calculer la repr�sentation en chaine de
   *  caract�res du plateau
   * @param IdOfPicesToNotDisplay  id of the pieces to
   *                               not display ; if eq.
   *                               -1, all the pieces
   *                               will displayed
   * @return l'affichage des positions utilisable
   */
  public String toString(int IdOfPicesToNotDisplay) {
    char[] retrunedPlateau = patron.toCharArray();

    for (int verticalCounter = 0; verticalCounter <
      LENGTH_HEIGHT; ++verticalCounter)
      for ( int horizontalCounter = 0; horizontalCounter
        < LENGTH_WIDTH ; ++horizontalCounter)
        if( this.matriceCellule[verticalCounter
          ][horizontalCounter] == SPOT )
          try {
            // juste pour pouvoir compiler, mais
            // c'est imposible que �a l�ve une erreur :
            // les valuers int�gr�s sont >0
            retrunedPlateau[
              getPositionsInCharArray(
                new Position(
                  horizontalCounter,
                  verticalCounter
                )
              )
              ] = SPOT_CHAR;
          } catch (Exception e) {}// finally pour
    // pouvoir compiler

    Position[] positionsPiece;
    for (int i = 0, c = pieceListe.size(); i < c ; i++) {
      Piece piece;
      if( IdOfPicesToNotDisplay != i ) {
        piece = pieceListe.get(i);
        positionsPiece = piece.getPos();
        retrunedPlateau[
          getPositionsInCharArray(positionsPiece[0])
          ] = piece.getCouleur().getLettre();
        retrunedPlateau[
          getPositionsInCharArray(positionsPiece[1])
          ] = piece.getCouleur().getLettre();
      }

    }

    return new String(retrunedPlateau);
  }

  /**
   * va afficher calculer la repr�sentation en chaine
   * de caract�res du plateau
   * @return l'affichage des positions utilisable
   */
  public String toString() {
    return this.toString(-1);
  }

  /**
   * calcule la position du caract�re a changer dans une
   * chaine de caract�res tel que le patron � partir
   * d'une position dans la matrice
   * @param p  position sur le plateau
   * @return la position dans la chaine de caract�re
   */
  private int getPositionsInCharArray( Position p ) {
    final int NB_LIGNE_CASE = 4,
      LIGNE_POUR_CENTRER = 2,
      CHAR_POUR_CENTRER = 2,
      NUMBER_OF_CHARS_IN_LINES = 14,
      NB_CHARS_CASE = 4;
    return (
      (
        (p.getY() + 1) * NB_LIGNE_CASE - LIGNE_POUR_CENTRER
      ) * NUMBER_OF_CHARS_IN_LINES)
      +
      (
        (p.getX() + 1) * NB_CHARS_CASE - CHAR_POUR_CENTRER
      );
  }


  /**
   * @return la pi�ce courrante a jouer
   */
  public Piece currentPiece() {
    return this.pieceListe.get(this.currentPiece);
  }

  /**
   * passe au tour suivant et scan les positions possibles
   */
  private void nextTour() {
    switch(((int) (++this.tour % 4))) {
      case 3:
        this.currentPiece = 1;
        break;

      default:
        this.currentPiece = ((int) (this.tour % 4));
    }
    // la formule �tait l� juste pour �viter des
    // conditions, or elle est 130 fois plus long que le
    // morceau de code ci-dessus...
    //this.currentPiece = 1 - (int) Math.cos( ( ++this
    // .tour * Math.PI ) /2. );

    // puis on scan les positions possibles
    this.scanPossiblePositions();
  }

  /**
   * calcul si la partie est finie
   * @return true si la parti est finie, false sinon
   */
  public boolean partieEstFinie() {
    for( Piece piece : this.pieceListe)
      if( !piece.getCouleur().equals(BLANC) && piece
        .getPoint() >= 4 )
        return true;

    return false;
  }

  /**
   * scan les positions possibles
   */
  private void scanPossiblePositions() {
    this.possiblePositions = new ArrayList<Position[]>();
    HashMap<Integer, Position> nonPossiblePositions;
    nonPossiblePositions = new HashMap<Integer, Position>();
    Position[] positions;
    Couleur c = this.currentPiece().getCouleur();

    for( Piece p : this.pieceListe) {
      if( p.getCouleur().equals(c) )
        continue;

      positions = p.getPos();

      nonPossiblePositions.put(
        positions[0].hashCode(),
        positions[0]
      );
      nonPossiblePositions.put(
        positions[1].hashCode(),
        positions[1]
      );
    }

    Position currentTestPosition;
    Position[]
      currentTestPositionArrayToAddInThePossiblePositions;
    currentTestPositionArrayToAddInThePossiblePositions
      = new Position[2];


    for (int vertical = LENGTH_HEIGHT - 1; vertical >= 0
      ; --vertical) { // pour commencer du bas
      for (int horizontal = 0; horizontal < LENGTH_WIDTH
        ; ++horizontal) {
        try {
          currentTestPosition = new Position(horizontal,
            vertical);

          currentTestPositionArrayToAddInThePossiblePositions
            [0] = currentTestPosition;

          if(
            !nonPossiblePositions.containsKey
              (currentTestPosition.hashCode())
              &&
              // si la case du haut est encore dans le
              // plateau
              vertical - 1 >= 0
              &&
              !nonPossiblePositions.containsKey(new
                Position(horizontal, vertical - 1).hashCode())
              &&
              !(
                this.currentPiece().getPos()[0].equals
                  (currentTestPosition)
                  &&
                  this.currentPiece().getPos()[1].equals
                    (new Position(horizontal, vertical
                      - 1))
              )
            ) {
            currentTestPositionArrayToAddInThePossiblePositions
              [1] = new Position(horizontal, vertical - 1);
            this.possiblePositions.add(
              currentTestPositionArrayToAddInThePossiblePositions
                .clone()
            );

            // on cr�� un autre tableau pour �viter de
            // r��crire le tableau ins�r�
            currentTestPositionArrayToAddInThePossiblePositions
              = new Position[2];
            currentTestPositionArrayToAddInThePossiblePositions
              [0] = currentTestPosition;
          }

          if(
            !nonPossiblePositions.containsKey
              (currentTestPosition.hashCode())
              &&
              horizontal + 1 < LENGTH_HEIGHT
              &&
              !nonPossiblePositions.containsKey(new
                Position(horizontal + 1, vertical)
                .hashCode())
              &&
              !(
                this.currentPiece().getPos()[0].equals
                  (currentTestPosition)
                  &&
                  this.currentPiece().getPos()[1].equals
                    (new Position(horizontal + 1,
                      vertical))
              )
            ) {
            currentTestPositionArrayToAddInThePossiblePositions
              [1] = new Position(horizontal + 1, vertical);
            this.possiblePositions.add(
              currentTestPositionArrayToAddInThePossiblePositions
            );

            currentTestPositionArrayToAddInThePossiblePositions
              = new Position[2];
          }
        }
        catch (Exception e) {}
      }
    }

  }

  /**
   * envois le plateau en chaine de caract�re, avec la
   * positions des pi�ces, sauf celle de la pi�ce
   * courante, avec un num�ro correspondant aux
   * mouvements possibles
   * @return la chaine en question
   */
  public String toStringWithPossiblePositions() {
    char[] retrunedPlateau;
    retrunedPlateau = this.toString(this.currentPiece)
      .toCharArray();
    Position[] pos;
    int posChar;

    // attribution des positions das la chaine de caract�res
    for (int i = 0, c = this.possiblePositions.size(); i
      < c ; ++i) {
      pos = this.possiblePositions.get(i);
      posChar = getPositionsInCharArray(pos[0]);

      if( Plateau.isInt("" + retrunedPlateau[posChar]) ) {
        retrunedPlateau[ posChar - 1 ] =
          retrunedPlateau[posChar];
        retrunedPlateau[ posChar ] = '-';
        retrunedPlateau[ posChar + 1 ] = Character
          .forDigit(i + 1, 10);
      }
      else
        retrunedPlateau[posChar] = Character.forDigit(i
          + 1, 10);
    }

    return new String(retrunedPlateau);
  }

  /**
   *  permet d'effectuer une d�placement de pi�ce sur le
   *  plateau
   *  @param posID  id ds le tableau this
   *  .possiblePosition ; commence � 0
   *  @return renvois le nombre de points gagn�s
   *  @throws Exception
   */
  public int makeMove(int posID) throws Exception {
    Position[] pos = this.possiblePositions.get(posID - 1);

    int pointsGagn�s = this.currentPiece().setPos(pos,
      this.matriceCellule);

    this.nextTour();

    return pointsGagn�s;
  }

  public int getNumberOfPossiblePosition() {
    return this.possiblePositions.size();
  }

  /**
   * renvois un tableau r�capitulatif des points
   * @return le tableau
   */
  public String getPointsRecap() {
    StringBuilder table = new StringBuilder();

    table.append("| Joueur n� |   Points   |\n");
    // le prochian retour � la ligne serra mis avant la
    // prochaine ligne
    table.append("|-----------|------------|");

    for( Piece p : this.pieceListe ) {
      if( p.getCouleur().equals(BLANC) )
        continue;

      table.append("\n|     ");
      table.append(p.getCouleur().getIDJoueur());
      table.append("     |     ");
      table.append(String.format("%2d", p.getPoint()));
      table.append("     |");
    }

    return table.toString();
  }

  /**
   * d�termine le gagnant
   * @return la pi�ce du gagnant
   * @throws Exception
   */
  public Piece getWinner() throws Exception {
    if( !this.partieEstFinie() )
      throw new Exception("La partie n'est pas finie");

    Piece pieceSuivante;
    Piece piece;
    piece = pieceListe.get(0);

    for (int i = 1, c = pieceListe.size(); i < c; i++) {
      pieceSuivante = pieceListe.get(i);

      if(
        piece.getCouleur().equals(BLANC)
          || pieceSuivante.getCouleur().equals(BLANC))
        continue;

      if( pieceSuivante.getPoint() >= 2
        && piece.getPoint() >= 4 )
        return piece;
      else if( pieceSuivante.getPoint() < 2
        && piece.getPoint() >= 4 )
        return pieceSuivante;

      piece = pieceSuivante;
    }
    return pieceListe.get(0);
  }




  /**
   * d�termine si un int est pr�sent dans une chaine
   * @param s    la chaine dans laquelle on recherche le int
   * @return true si un int est pr�sent dans le chaine
   */
  public static boolean isInt(String s) {
    return isInt(s, 10);
  }

  /**
   * d�termine si un int est pr�sent dans une chaine
   * @param s        la chaine dans laquelle on recherche
   *                 le int
   * @param radix    base de num�ration du int
   * @return true si un int est pr�sent dans le chaine
   */
  public static boolean isInt(String s, int radix) {
    if(s.isEmpty()) return false;
    for(int i = 0; i < s.length(); ++i) {
      if(i == 0 && s.charAt(i) == '-') {
        if(s.length() == 1) return false;
        else continue;
      }
      if(Character.digit(s.charAt(i),radix) < 0)
        return false;
    }
    return true;
  }

  /**
   * retrouve le prochain int dans un scanner et imprime
   * les message d'erreur dans un printStream
   * @param sc     le scanner � scanner #obvious
   * @param out    le printStream o� l'on imprime les
   *               messages d'erreur
   * @return le prochain int scann�
   */
  public static int getNextInt( Scanner sc, PrintStream
    out) {
    return getNextInt(sc, out, null, null);
  }

  /**
   * retrouve le prochain int dans un scanner et imprime
   * les message d'erreur dans un printStream
   * @param sc     le scanner � scanner #obvious
   * @param out    le printStream o� l'on imprime les
   *               messages d'erreur
   * @param min    si non null, le minimum a scanner
   * @param max    si non null, le maximum a scanner
   * @return le prochain int scann�
   */
  public static int getNextInt( Scanner sc, PrintStream
    out, Integer min, Integer max ) {
    while( !sc.hasNextInt() ) {
      out.println("Veuillez indiquer un nombre");
      sc.next();
    }

    int intSaisi = sc.nextInt();
    if( min != null && max != null && ( min > intSaisi ||
      max < intSaisi ) ) {
      out.println("Veuillez indiquer un nombre comprit " +
        "entre " + min + " et " + max);
      return getNextInt(sc, out, min, max);
    }

    return intSaisi;
  }
}

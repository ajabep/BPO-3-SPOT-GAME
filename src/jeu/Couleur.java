/*
 * Created by J-B on 06/02/2015.
 */

package jeu;

public enum Couleur {
  BLANC('W', 0, "blanche"),
  BLEU('B', 2, "bleu"),
  ROUGE('R', 1, "rouge");

  /**
   * lettre pour l'identification de la pi�ce lors de
   * l'affichage
   */
  private final char lettre;

  /**
   * num�ro pour identification du joueur a qui
   * appartient la pi�ce
   */
  private final int IDJoueur;

  /**
   * nom de la couleur de la pi�ce (au f�minin)
   */
  private final String nom;

  Couleur(char lettre, int IDJoueur, String nom) {
    this.lettre = lettre;
    this.IDJoueur = IDJoueur;
    this.nom = nom;
  }

  public char getLettre() {
    return lettre;
  }
  public int getIDJoueur() {
    return IDJoueur;
  }

  /**
   * getter du nom
   */
  public String toString() {
    return this.nom;
  }
}
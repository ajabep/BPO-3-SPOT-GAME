/*
 * Created by J-B on 05/02/2015.
 */

package jeu;

import static jeu.Cellule.*;

/**
 * Type de donn�e repr�sentant une pi�ce sur le plateau
 */
public class Piece {

  /**
   * couleur de la pi�ce
   */
  private Couleur couleur;

  /**
   * Tableau de 2 positions (car il n'y a que 2 cases)
   */
  private Position[] pos;

  /**
   * nombre de points gagn�s
   */
  private int point;


  public Piece(Position[] pos, Couleur couleur)
    throws Exception {
    this.setPos(pos) ;
    this.couleur = couleur;
    this.point = 0;
  }

  /**
   * getter de la couleur de la pi�ce
   */
  public Couleur getCouleur() {
    return couleur;
  }

  /**
   * getter du tableau de position
   */
  public Position[] getPos() {
    return pos;
  }

  /**
   * le setter du tableau de positions
   * @param pos    un tableau de 2 positions contig�e
   * @throws Exception
   */
  public void setPos(Position[] pos) throws Exception {
    if( pos.length != 2 )
      throw new Exception("pos must have 2 positions");

    if( ! pos[0].isContiguousTo(pos[1]) )
      throw new Exception("The 2 positions in pos must be" +
        " contiguous");

    this.pos = pos.clone();
  }

  /**
   * le setter du tableau de positions et ajoute les
   * points m�rit�s
   * @param pos        un tableau de 2 positions contig�e
   * @param matrice    matrice de cellule avec les spots
   * @return le nombre de spots recouvert (donc le
   * nombre de points ajout�)
   * @throws Exception
   */
  public int setPos(Position[] pos, Cellule[][] matrice)
    throws Exception {
    this.setPos(pos);
    return this.addPoints(matrice);
  }

  // oui, �tant tr�s p�simiste, je ne met pas de `s` :p
  public int getPoint() {
    return this.point;
  }

  /**
   * ajoute des points en fonction du nombre de spots
   *  recouverts
   * @param matrice  la repr�sentation du plateau �
   *                  travers une matrice de Cellule
   * @return le nombre de points ajout�
   */
  private int addPoints( Cellule[][] matrice ) {
    int pointsToAdd = this.getNumberOfSpot( matrice );
    this.point += pointsToAdd;
    return pointsToAdd;
  }

  /**
   * calcul le nombre de spots recouvert
   * @param matrice  la repr�sentation du plateau �
   *                  travers une matrice de Cellule
   * @return le nombre de spots
   */
  public int getNumberOfSpot( Cellule[][] matrice ) {
    int numbreOfSpot = 0;

    for( Position p : this.pos)
      if( matrice[p.getY()][p.getX()] == SPOT )
        ++numbreOfSpot;


    return numbreOfSpot;

  }
}

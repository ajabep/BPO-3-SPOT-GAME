package jeu;

import org.junit.Test;

import static org.junit.Assert.*;
import static jeu.Couleur.*;

public class CouleurTest {

  @Test
  public void testGetLettre() throws Exception {
    Couleur blanc = BLANC;
    Couleur bleu = BLEU;
    Couleur rouge = ROUGE;

    assertEquals(blanc.getLettre(), 'W');
    assertEquals(bleu.getLettre(), 'B');
    assertEquals(rouge.getLettre(), 'R');
  }

  @Test
  public void testGetIDJoueur() throws Exception {
    Couleur blanc = BLANC;
    Couleur bleu = BLEU;
    Couleur rouge = ROUGE;

    assertEquals(blanc.getIDJoueur(), 0);
    assertEquals(bleu.getIDJoueur(), 2);
    assertEquals(rouge.getIDJoueur(), 1);
  }

  @Test
  public void testToString() throws Exception {
    Couleur blanc = BLANC;
    Couleur bleu = BLEU;
    Couleur rouge = ROUGE;

    assertEquals(blanc.toString(), "blanche");
    assertEquals(bleu.toString(), "bleu");
    assertEquals(rouge.toString(), "rouge");
  }
}

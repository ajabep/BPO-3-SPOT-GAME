package jeu;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class PositionTest {

  @Test
  public void testGetX() throws Exception {
    Position p1 = new Position(1, 2);
    Position p2 = new Position(5, 20);

    assertEquals(p1.getX(), 1);
    assertEquals(p2.getX(), 5);
  }

  @Test
  public void testGetY() throws Exception {
    Position p1 = new Position(1, 2);
    Position p2 = new Position(5, 20);

    assertEquals(p1.getY(), 2);
    assertEquals(p2.getY(), 20);

  }

  @Test
  public void testIsContiguousTo() throws Exception {
    Position p1 = new Position(1, 2);
    Position p2 = new Position(1, 2); // = p1
    Position p3 = new Position(2, 2); // contigue � p1
    Position p4 = new Position(2, 3); // pas contigue � p1
    Position p5 = new Position(1, 3); // contigue � p1
    Position p6 = new Position(5, 20); // pas contigue � p1

    assertFalse(p1.isContiguousTo(p1));

    assertFalse(p1.isContiguousTo(p2));
    assertFalse(p2.isContiguousTo(p1));

    assertTrue(p1.isContiguousTo(p3));
    assertTrue(p3.isContiguousTo(p1));

    assertFalse(p1.isContiguousTo(p4));
    assertFalse(p4.isContiguousTo(p1));

    assertTrue(p1.isContiguousTo(p5));
    assertTrue(p5.isContiguousTo(p1));

    assertFalse(p1.isContiguousTo(p6));
    assertFalse(p6.isContiguousTo(p1));
  }

  @Test
  public void equals() throws Exception {

    Position p1 = new Position(1, 2);
    Position p2 = new Position(1, 2);


    assertTrue(p1.equals(p2));

    HashMap<Integer, Position> hashMap;
    hashMap =new HashMap<Integer, Position>();

    hashMap.put(p1.hashCode(), p1);

    assertTrue(hashMap.containsKey(p2.hashCode()));



  }
}
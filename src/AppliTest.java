import org.junit.Test;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;
import static org.junit.Assert.*;

import jeu.Plateau;



public class AppliTest {

  @Test
  public void testGetNextInt() throws Exception {
    String toScan = "a g2 5e 6 20 8 3 10 9";
    Scanner sc = new Scanner(toScan);
    FileOutputStream fout = new FileOutputStream(
      "src/META-INF/out.test", false);
    PrintStream pout = new PrintStream(fout);

    assertEquals(
      Plateau.getNextInt(sc, pout, 8, 10),
      8
    );

    assertEquals(
      Plateau.getNextInt(sc, pout, 8, 10),
      10
    );

    assertEquals(
      Plateau.getNextInt(sc, pout, 8, 10),
      9
    );



    toScan = "a g2 5e 6 20 8 3 10 9";
    sc = new Scanner(toScan);

    assertEquals(
      Plateau.getNextInt(sc, pout),
      6
    );

    assertEquals(
      Plateau.getNextInt(sc, pout),
      20
    );

    assertEquals(
      Plateau.getNextInt(sc, pout),
      8
    );

    assertEquals(
      Plateau.getNextInt(sc, pout),
      3
    );

    assertEquals(
      Plateau.getNextInt(sc, pout),
      10
    );

    assertEquals(
      Plateau.getNextInt(sc, pout),
      9
    );
  }

  @Test
  public void testIsInt() throws Exception {
    // In decimal
    assertEquals( // an octal digit
      Plateau.isInt("0316427", 10),
      Plateau .isInt("0316427")
    );
    assertEquals( // a decimal digit
      Plateau.isInt("32578", 10),
      Plateau.isInt("32578")
    );
    assertEquals( // a float
      Plateau.isInt("0.354", 10),
      Plateau.isInt("0.354")
    );
    assertEquals( // a hex digit
      Plateau.isInt("5a6d7", 10),
      Plateau.isInt("5a6d7")
    );
    assertEquals( // an other notation of a hex digit
      Plateau.isInt("0x5a6d7", 10),
      Plateau.isInt("0x5a6d7")
    );
    assertEquals( // not a number
      Plateau.isInt("sg", 10),
      Plateau.isInt("sg")
    );

    // In decimal
    assertEquals( // an octal digit
      Plateau.isInt("0316427"),
      true
    );
    assertEquals( // a decimal digit
      Plateau.isInt("32578"),
      true
    );
    assertEquals( // a float
      Plateau.isInt("0.354"),
      false
    );
    assertEquals( // a hex digit
      Plateau.isInt("5a6d7"),
      false
    );
    assertEquals( // an other notation of a hex digit
      Plateau.isInt("0x5a6d7"),
      false
    );
    assertEquals( // not a number
      Plateau.isInt("sg"),
      false
    );

    // In octal
    assertEquals( // an octal digit
      Plateau.isInt("0316427", 8),
      true
    );
    assertEquals( // a decimal digit
      Plateau.isInt("32578", 8),
      false
    );
    assertEquals( // a float
      Plateau.isInt("0.354", 8),
      false
    );
    assertEquals( // a hex digit
      Plateau.isInt("5a6d7", 8),
      false
    );
    assertEquals( // an other notation of a hex digit
      Plateau.isInt("0x5a6d7", 8),
      false
    );
    assertEquals( // not a number
      Plateau.isInt("sg", 8),
      false
    );

    // In hexa
    assertEquals( // an octal digit
      Plateau.isInt("0316427", 16),
      true
    );
    assertEquals( // a decimal digit
      Plateau.isInt("32578", 16),
      true
    );
    assertEquals( // a float
      Plateau.isInt("0.354", 16),
      false
    );
    assertEquals( // a hex digit
      Plateau.isInt("5a6d7", 16),
      true
    );
    assertEquals( // an other notation of a hex digit
      Plateau.isInt("0x5a6d7", 16),
      false
    );
    assertEquals( // not a number
      Plateau.isInt("sg", 16),
      false
    );
  }
}
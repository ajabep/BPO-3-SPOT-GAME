/*
 * Created by J-B on 05/02/2015.
 */

import jeu.Piece;
import jeu.Plateau;

import java.io.*;
import java.util.Scanner;

/** Classe principale */
public class Appli {

  public static void main(String[] argc) {

    try {
      Plateau plateau = new Plateau();
      Piece p;
      Scanner sc = new Scanner(System.in);
      int pointsWon = 0;
      System.out.println("Lancement du jeu");

      while (!plateau.partieEstFinie()) { // partie finie

        p = plateau.currentPiece();

        if (!p.getCouleur().name().equals("BLANC")) {
          System.out.println("Joueur " +
                             p.getCouleur().getIDJoueur() +
                             ", � toi: ");
        }


        //System.out.println("affichage du jeu :");
        System.out.println(plateau.toString());

        /*Joueur joue*/
        System.out.println("Positions possibles :");
        System.out.println(
          plateau.toStringWithPossiblePositions());

        System.out.print(" ou veux tu bouger la pi�ce "
          + p.getCouleur().toString() + " ? ");
        int i = Plateau.getNextInt(sc, System.out, 1,
          plateau.getNumberOfPossiblePosition());

        if (p.getCouleur().name().equals("BLANC")) {
          plateau.makeMove(i);
          System.out.println("\n");
          System.out.println("Vous venez gagner " +
            pointsWon + " points");
          System.out.println("R�capitulatif des points");
          System.out.println(plateau.getPointsRecap());
          pointsWon = 0;
        }
        else {
          pointsWon = plateau.makeMove(i);
          System.out.print("\n");
        }
      }
      sc.close();

      Piece winner = plateau.getWinner();
      System.out.println("Nous avons un gagnant.");
      Thread.sleep(500); // les pauses pour le suspense
      System.out.print("Et le gagnant est ......");
      Thread.sleep(600);
      System.out.println(" le joueur " +
        winner.getCouleur().getIDJoueur() + " avec " +
        winner.getPoint() + " points !");
      System.out.println("Bravo joueur " +
        winner.getCouleur().getIDJoueur() + " !");
      System.out.println(
        "Ton adversaire �tait brave et courageux, mais " +
          "malgr�s les dangers bien cach�s, " +
          " tu as su faire preuve de courage ! Bravo !");


      // oui, je m'ammuse bien ^^
      System.out.println("\n");
      System.out.println("BBBBBBBBBBBBBBBBB   " +
        "RRRRRRRRRRRRRRRRR                  AAA   " +
        "VVVVVVVV           VVVVVVVV     OOOOOOOOO     ");
      System.out.println("B::::::::::::::::B  " +
        "R::::::::::::::::R                A:::A  " +
        "V::::::V           V::::::V   OO:::::::::OO   ");
      System.out.println("B::::::BBBBBB:::::B " +
        "R::::::RRRRRR:::::R              A:::::A " +
        "V::::::V           V::::::V OO:::::::::::::OO ");
      System.out.println("BB:::::B     B:::::BRR:::::R  " +
        "   R:::::R            A:::::::AV::::::V        " +
        "   V::::::VO:::::::OOO:::::::O");
      System.out.println("  B::::B     B:::::B  R::::R  " +
        "   R:::::R           A:::::::::AV:::::V        " +
        "   V:::::V O::::::O   O::::::O");
      System.out.println("  B::::B     B:::::B  R::::R  " +
        "   R:::::R          A:::::A:::::AV:::::V       " +
        "  V:::::V  O:::::O     O:::::O");
      System.out.println("  B::::BBBBBB:::::B   " +
        "R::::RRRRRR:::::R          A:::::A " +
        "A:::::AV:::::V       V:::::V   O:::::O     " +
        "O:::::O");
      System.out.println("  B:::::::::::::BB    " +
        "R:::::::::::::RR          A:::::A   " +
        "A:::::AV:::::V     V:::::V    O:::::O     " +
        "O:::::O");
      System.out.println("  B::::BBBBBB:::::B   " +
        "R::::RRRRRR:::::R        A:::::A     " +
        "A:::::AV:::::V   V:::::V     O:::::O     " +
        "O:::::O");
      System.out.println("  B::::B     B:::::B  R::::R  " +
        "   R:::::R      A:::::AAAAAAAAA:::::AV:::::V " +
        "V:::::V      O:::::O     O:::::O");
      System.out.println("  B::::B     B:::::B  R::::R  " +
        "   R:::::R     A:" +
        "::::::::::::::::: :::AV:::::V:::::V       " +
        "O:::::O     O::::O");
      System.out.println("  B::::B     B:::::B  R::::R  " +
        "   R:::::R    A::" +
        ":::AAAAAAAAAAAAA:::::AV:::::::::V        " +
        "O::::::O   O::::::O");
      System.out.println("BB:::::BBBBBB::::::BRR:::::R  " +
        "   R:::::R   A:::::A             " +
        "A:::::AV:::::::V         O:::::::OOO:::::::O");
      System.out.println("B:::::::::::::::::B R::::::R  " +
        "   R:::::R  A:::::A               " +
        "A:::::AV:::::V           OO:::::::::::::OO ");
      System.out.println("B::::::::::::::::B  R::::::R  " +
        "   R:::::R A:::::A                 A:::::AV:::V" +
        "              OO:::::::::OO   ");
      System.out.println("BBBBBBBBBBBBBBBBB   RRRRRRRR  " +
        "   RRRRRRRAAAAAAA                   AAAAAAAVVV " +
        "                OOOOOOOOO     ");
      System.out.println("\n");
    }
    catch ( Exception e ) {
      try {
        PrintWriter errorLogFile = new PrintWriter(
          new FileOutputStream("err.log"));

        errorLogFile.print("\n");
        errorLogFile.println("[ERROR] Une erreur dans le" +
          " programme a �t� d�tect� (de type " +
          e.getClass().getName() + ") : ");
        errorLogFile.println( e.getMessage() );
        errorLogFile.println("Stack trace :");
        e.printStackTrace(errorLogFile);

        errorLogFile.close();
        System.out.print("\n\n\n\n\n");
        System.out.println("[ERROR] Une erreur est " +
          "survenue lors de l'execution du programme");
        System.out.println("Pour plus de d�tails, " +
          "consultez le log d'erreur");
      }
      catch (FileNotFoundException f) {
        System.out.print("\n\n\n\n\n");
        System.out.println("[ERROR] Impossible d'�crire " +
          "dans le fichier de log d'erreur");
        System.out.println("[ERROR] Une erreur dans le " +
          "programme a �t� d�tect� : ");
        System.out.println( e.getMessage() );
        System.out.println("Stack trace :");
        e.printStackTrace();
      }
    }
  }

}

#!/bin/bash

# Copyright (c) Ajabep
# under WTFPL
# See http://www.wtfpl.net/ for more details.

if [ "$1" == "" ]
then
	echo "type '$0 -h' to display the help"
	echo ""
	echo ""
fi

if [ "$1" == "-h" ]
then
	echo "usage: $0"
	echo "   or: $0 <option>"
	echo ""
	echo "will push the repo"
	echo ""
	echo ""
	echo "Options:"
	echo "    -h                                                       display the help"
	echo "    -c <commitMessage> [-t <tagName> [<tagMessage>]] [-np]   add all files and commit all with the message <commitMessage>"
	echo "                                                             if the option '-t' is given, create the tag <tagName> with the message <tagMessage>"
	echo "                                                             if the option '-np' is given, will not push the commit"
	exit
fi



if [ "$1" == "-c" ]
then
	
	echo ''
	echo 'Adding all new files'
	git add --all

	echo ''
	echo ''
	echo 'Commit all changes'
	# $2$ : messageCommit
	git commit -m "$2"

	if [ "$3" == "-t" ]
	then
		echo ''
		echo ''
		echo 'Adding tags'
		# $4 : tagName
		# $5 : tagMessage
		if [ "$5" == "-np" ] || [ "$5" == "" ]
		then
			git tag -a "$4"
		else
			git tag -a "$4" -m "$5"
			shift # $6 -> $5
		fi
		
		shift # $5 -> $4
		shift # $4 -> $3
		shift # $3 -> $2
	fi

	echo ''
	echo ''
fi

if [ "$3" != "-np" ]
then

	echo 'synch'
	git push origin master --tags

	if [ $? != 0 ]
	then
		echo ''
		echo ''
		echo 'You are not up-to-date'
		echo 'You will be update'
		git pull origin master

		if [ $? == 0 ]
		then
			echo 'Now, you are up-to-date'
		else
			echo '[ERROR] An error occcured when the update'
			exit
		fi
		
		echo ''
		echo ''
		echo 're-synch'
		git push origin master --tags

		if [ $? == 0 ]
		then
			echo 'The synch is a success'
		else
			echo '[ERROR] The synch had an error'
		fi
	else
		echo ''
		echo ''
		echo 'You are up-to-date'
		echo 'The synch is a success'
	fi
fi
